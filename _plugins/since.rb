module Jekyll
  module SinceFilter
    def since(stop, start)
      startt = Time.at(start).utc
      stopt = Time.at(stop).utc

      years = stopt.year - startt.year
      months = stopt.month - startt.month
      startt_into_month = startt.day * 24 * 60 * 60 + startt.hour * 60 * 60 + startt.min * 60 + startt.sec
      stopt_into_month = stopt.day * 24 * 60 * 60 + stopt.hour * 60 * 60 + stopt.min * 60 + stopt.sec
      secs = stopt_into_month - startt_into_month

      # first propagate time/month/year offsets

      if secs < 0
        months -= 1
      end

      if months < 0
        years -= 1
        months += 12
      end

      # adjust time difference across month boundaries

      if secs < 0
        adjusted_year = startt.year + years
        adjusted_month = startt.month + months
        if adjusted_month > 12
          adjusted_year += 1
          adjusted_month -= 12
        end
        startt_adjusted = Time.utc(adjusted_year, adjusted_month, startt.day, startt.hour, startt.min, startt.sec)
        secs = (stopt - startt_adjusted).floor
      end

      seclen = 1
      minlen = 60 * seclen
      hourlen = 60 * minlen
      daylen = 24 * hourlen
      weeklen = 7 * daylen

      weeks = secs / weeklen
      secs -= weeks * weeklen
      days = secs / daylen
      secs -= days * daylen
      hours = secs / hourlen
      secs -= hours * hourlen
      mins = secs / minlen
      secs -= mins * minlen

      output = Array.new

      output << "#{years}y" if years > 0
      output << "#{months}mo" if months > 0
      output << "#{weeks}w" if weeks > 0
      output << "#{days}d" if days > 0
      output << "#{hours}h" if hours > 0
      output << "#{mins}m" if mins > 0
      output << "#{secs}s" if secs > 0

      output.join(" ")
    end
  end
end

Liquid::Template.register_filter(Jekyll::SinceFilter)
