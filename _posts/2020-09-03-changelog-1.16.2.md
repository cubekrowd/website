---
layout: blog_post
title: "1.16.2 is live on CubeKrowd!"
category: community
author: JohnStar128
short: "1.16.2 is live on CubeKrowd! Hide your gold!"
tags:
  - server
  - update
---

We're pleased to announce CubeKrowd is now running on Minecraft 1.16.2! With this update, we've brought a whole host of new features, changes and all around improvements! As usual please report any and all issues you encounter.

<!--more-->
## General

- Updated all servers to Minecraft 1.16.2. Players must connect with a 1.16.2 client.
- Added `/helpop`, message all Staff members **on any server** 
- Updated the [Approved Mods](https://cubekrowd.net/link/mods) document
- Added new trails to `/trails` in Lobby, Gamelobby, Creative, BuildComp and minigames



## Lobby

- Added a timer to all the parkour courses in the Lobby world.
    - Each pressure plate throughout a course indicates a checkpoint.
    - `/parkour play <course name>` will take you directly to a course. Step on the pressure plate at the start of a course to begin the timer.
    - `/parkour last-checkpoint` returns you to your last checkpoint.
    - `/parkour leave` will quit out of the course.
    - `/parkour resume <course name>` will let you resume from your last checkpoint if you left the course.
    - `/parkour records <course name>` will let you see your personal best times for the specified course.
- Added the new minigame **CubeKarts**
- Added three new Dropper minigames
- Added several new advancements for hidden areas
- Use the compass navigator to get to the minigames quickly

## Creative

- Increased the `/sethome` limits
    - Trusted: 3 -> 10
    - Respected: 5 -> 15
    - Builder: 10 -> 20
    - Helpers: 10 -> 50
    - Donator 1: 1 -> 10
    - Donator 2: 1 -> 20

- Changed some behaviors in our item filtering plugin
    - Improved potion effect filtering
    - ClickEvents are filtered out of book data
    - Entity Data is cleared out of spawn eggs

- There's been reported issues of redstone machines breaking in this update. If you notice anything broken, please report it

## Skyblock

- Added new challenges
- Added new greenhouses
- Greenhouses are now dimension-dependent. Nether greenhouses can only be made in the nether, and overworld greenhouses can only be made in the overworld. With the exception of The End greenhouse, being made only in the overworld.
- Reverted fishing loot to how it was in 1.15

## Survival

- Expanded the overworld world border to 14,000 in diameter
- Expanded the nether world border to 5,000 in diameter
- Books can now have color codes in them again. Use (&) instead of the section symbol (§)
- When enough players are sleeping to skip the night, time will pass with a fancy visual effect
- There's been reported issues of redstone machines breaking in this update. If you notice anything broken, please report it
- Some structures such as villages may generate incorrectly.

## MissileWars

- Adjusted the mining speed of pistons to better match 1.15. It's not the most perfect fix, but we hope it makes a difference

## PVP

- Server will be temporarily offline while undergoing maintenance

