---
layout: blog_post
title: "1.19.3 is live on CubeKrowd!"
category: community
author: TraksAG
short: "But without Mojang chat reporting."
tags:
  - server
  - update
---

CubeKrowd just got updated to 1.19.3. Here is a list of all the changes that came with the update. As usual please report any and all issues you encounter. Minecraft 1.19.3 may seem like a small update, but Mojang made a lot of changes under the hood.

<!--more-->
## General

- Updated all servers to Minecraft 1.19.3. You need a 1.19.3 client to join.
- Note that Mojang chat reporting does *not* work on CubeKrowd, as we're not sure how easy it is to abuse their system. It would also require quite some technical work to adopt it. Beware that our own chat rules still apply: [tha rulez](/rules/).
- Chat signing is *not* required on CubeKrowd, so you can join with mods such as No Chat Reports.
- Maybe fixed an issue where players would spuriously un-AFK upon joining the server.

## Creative & BuildComp

- More heads in the HeadDatabase `/hdb`.
- The OP items tab in the Creative mode inventory is available if you enable it under Options > Controls > Operator Items Tab.

## PVP

- O PVP, PVP, wherefore art thou PVP?
