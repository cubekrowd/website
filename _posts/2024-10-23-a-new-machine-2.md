---
layout: blog_post
title: "Migrating to a new machine 2.0"
category: developer
author: TraksAG
short: "Featuring the battle of the CubeBot."
tags:
  - server
---

*Image from Hetzner's website.*

CubeKrowd once again moved to a new machine! The upgrade happened a bit more than a month ago on the 8th of September, and everything is still running smoothly.

As with the upgrade of 4 years ago, which we wrote about [here](/blog/upgrading-server/), this is mostly a CPU upgrade with a significantly lower monthly cost. Inflation wasn't kind to our old subscription! The new machine is located in the same datacentre as our previous machine, so everyone's ping should be approximately the same.

<!--more-->
## New hardware

The new machine is hosted in Falkenstein, Germany by Hetzner, and has the following specifications.
- CPU: Intel i5-12500 (much better single-thread performance than the AMD Ryzen 5 3600 we had before)
- RAM: 64 GB DDR4 3200 MHz (we had 64 GB DDR4 before, but I believe a slightly lower frequency)
- Network bandwidth: 1 Gbit/s (same)
- Storage: 512 GB RAID 1 SSD (same)
- External backup: 1 TB (up from 512 GB)

## The migration process

Sooo... we have a confession to make, we were using an ancient operating system (Ubuntu 18 LTS from 2018) and ancient software.
Of course security patches were still applied, but it wasn't an ideal situation.
With the upgrade, we decided to move to a much more recent operating system: Ubuntu 24 LTS.

To transition to more up-to-date software, we essentially set up all the software on the new machine from scratch.
However, due to changes in newer software, some stuff broke: our backup scripts, Docker scripts, etc.
Especially CubeBot (our Discord bot) proved to be a real pain.
A great battle emerged to get CubeBot up and running: was it faster to set it up with Docker or with pyenv?
If you thought Docker, you lost!
We eventually managed to use pyenv to run CubeBot on an old Python version.
I guess some of the ancient software still remains...

Luckily all the databases and Minecraft worlds could be copied over easily using `rsync`, without any fuss.
